# Credit

Well, everyone may contribute to this by uploading pictures for the EmuGUI wiki. However, some things need to be organized.

## Rules

- Everyone who wants to contribute for the first time must create a new folder for him-/herself.
- The license CC BY-4.0 applies unless you put a specific license into your folder. If you want to make them public domain, you must, please, tell the user so in a license file as well. Just make sure it may be used by the EmuGUI wiki, please.
- If you want different licenses for different pictures, please make a new folder inside yours with the license that you want to apply. Just make sure it may be used by the EmuGUI wiki, please.
- Your picture must be in a format which is usable by Codeberg Repositories and Wikis. (PNGs are known to work)
- Your picture must be relatable to EmuGUI (or QEMU). However, you must not prove you ran this on QEMU (or with EmuGUI). Inappropriate pictures will be removed.

## Contributors and their default licenses

| Contributor | Folder | Default license |
| ----------- | ------ | --------------- |
| lucien-rowan | pics | CC BY-4.0 |